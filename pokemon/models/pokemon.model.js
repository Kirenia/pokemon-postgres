const ApiResponse = require('./api-response.models');
const { db } = require('../models/db.model');

class Pokemon {

    //Get
    async get() {
        const apiResp = new ApiResponse();
           
        try {

            const result = await db.pool.query(' SELECT * FROM pokemon');
            console.log(result);
            apiResp.data = result.rows || [];
            console.log(apiResp);

        } catch (e) {

            apiResp.status = 500;
            apiResp.error = e.message;
        }

        return apiResp;

    }

    async add(pokemon) {

        console.log(pokemon);
        const apiResp = new ApiResponse();
        if (pokemon === null) {
            apiResp.status == 400;
            apiResp.error = 'please enter a pokemon!';

            return apiResp;
        }
        const InsertDb = {
            text: `INSERT INTO pokemon (
                name,
                species,
                height,
                weight,
                abilities,
                type ,
                sprite,
                hp,
                sp_atk,
                sp_def,
                total,
                active)
                VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12) RETURNING id`,
            values: [pokemon.name, pokemon.species, pokemon.height, pokemon.weight, pokemon.abilities, pokemon.type,
            pokemon.sprite, pokemon.hp, pokemon.sp_atk, pokemon.sp_def, pokemon.total, pokemon.active]
        };
        try {
            const result = await db.pool.query(InsertDb);
            apiResp.data = {
                ...pokemon,
                id: result.rows[0].id
            } || null


        } catch (e) {
            apiResp.status = 500;
            apiResp.error = e.message;

        }
        return apiResp;


    }


    async update(pokemon, id) {
        console.log(pokemon, id);
        const apiResp = new ApiResponse();
        if (pokemon === null) {
            apiResp.status = 400;
            apiResp.error = 'Please enter a pokemon'
            return apiResp;

        }
        // const query = `UPDATE pokemon SET name=$1,species=$2,height=$3,weight=$4,abilities=$5,type=$6,sprite=7,
        // hp=$8,sp_atk=$9,sp_def=$10,total=$11,active=$12 WHERE id=$1 RETURNING id`;


        const updateQuery = {
            text: `UPDATE pokemon SET name=$1,species=$2,height=$3,weight=$4,abilities=$5,type=$6,sprite=$7,
            hp=$8,sp_atk=$9,sp_def=$10,total=$11,active=$12 WHERE id=id RETURNING id`,
            values: [pokemon.name, pokemon.species, pokemon.height, pokemon.weight, pokemon.abilities, pokemon.type,
            pokemon.sprite, pokemon.hp, pokemon.sp_atk, pokemon.sp_def, pokemon.total, pokemon.active]

        };
        try {
            const result = await db.pool.query(updateQuery);
            apiResp.data = {
                ...pokemon,
                id: result.rows[0].id
            } || null;

        } catch (e) {
            apiResp.status = 500;
            apiResp.error = e.message;


        }
        return apiResp;


    }



    async delete(id) {

            const apiResp = new ApiResponse();

        try {

            const result = await db.pool.query(`DELETE FROM pokemon WHERE id=$1 RETURNING id`, [id])
            console.log(result);
            apiResp.data = result.rows || [];



        }
        catch (e) {
            apiResp.status = 500;
            apiResp.error = e.message;

        }
        return apiResp;

    }

}


module.exports.pokemon = new Pokemon();