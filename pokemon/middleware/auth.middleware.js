
const { auth } = require('../models/auth.models');
const allowedRoutes = [
    '/api/v1/auth/generate-token'
];

async function authMiddleware(req, res, next) {

    if (allowedRoutes.includes(req.path)) {
        next();
        return;
    }

    //Extract the authorization header 
    const { authorization } = req.headers;
    const token = !authorization ? false : authorization.split(' ')[1];

    //Check that the token exist 
    if (!token) {
        const apiResp = auth.getUnauthorizaResp('No Auth token Exist', 401);
        res.status(apiResp.status).json(apiResp).end();

        return;
    }
    const tokenResult = await auth.validateToken(token);
    if (tokenResult) {
        next();


    } else {
        const apiResp = auth.getUnauthorizaResp('invalid token', 403);
        res.status(apiResp.status).json(apiResp).end();
        return;
    }
    //Valid:Allow the request to continue 

    //Invalis: Send a authorizatio
}
module.exports = authMiddleware;