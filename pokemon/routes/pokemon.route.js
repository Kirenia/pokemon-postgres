const express = require('express');
const router = express.Router();

const {pokemon} = require('../models/pokemon.model');

//const ApiResponse = require('../models/api-response.models');
//Get Pokemon
router.get('/pokemon', async(req, res) => {
    const pokemonResp= await pokemon.get();
    console.log(pokemonResp);
    res.status(pokemonResp.status).json(pokemonResp).end();
});


//Add Pokemon

router.post('/pokemon/add', async(req, res) => {
    console.log(req);
     const newPokemon = req.body || null;
     console.log(newPokemon);
     const pokemonResp= await pokemon.add(newPokemon);

     res.status(pokemonResp.status).send(pokemonResp).end();
  
});
//Update Pokemon

router.put('/pokemon/update/:id',async (req, res) => {
    const OldPokemon=req.body|| null;
    const id = req.params.id
    const pokResp= await pokemon.update(OldPokemon,id); 
    res.status(pokResp.status).send(pokResp).end();
});

//Delete Pokemon

router.delete('/pokemon/delete/:id', async(req, res) => {
    const id = parseInt(req.params.id); 
    console.log(id);
    const pokeResp= await pokemon.delete(id);
    console.log(pokeResp);
    res.status(pokeResp.status).send(pokeResp).end();
});




module.exports = router;

